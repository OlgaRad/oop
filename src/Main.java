
import UserData.User;
import UserData.UserChild;

public class Main {
    public static void main(String[] args) {
        User user1 = new User("Nick", "Nelson", 7487084, 380507455, "test@test.com", "testTest1" );
        user1.displayInfo();
        String email = user1.getEmail();
        System.out.println("Email: "+email);
        System.out.println("isValidEmail: "+user1.isValidEmail(email));
        String password = user1.getPassword();
        System.out.println("Password: "+password);
        System.out.println("isValidPassword: "+user1.isValidPassword());

        User user2 = new User("qwer@tycom.ua", "qwert");
        user2.displayInfoShort();
        String email2 = user2.getEmail();
        System.out.println("isValidEmail: "+user2.isValidEmail(email2));
        System.out.println("isValidPassword: "+user2.isValidPassword());

        UserChild userChild = new UserChild("John", "Jonson", 7541012, 785474545, "john@mail.ru", "johnJohn1",5,1000);
        userChild.displayInfo();
        double experience = userChild.getExperience();
        System.out.println("Experience: "+experience);
        double salary = userChild.getSalary();
        System.out.println("Salary: "+salary);
        System.out.println("RaisedSalary: "+userChild.raisedSalary(experience,salary)+ " USD");

        UserChild userChild1 = new UserChild("andrew@mail.ru", "andrew1");
        userChild1.displayInfoShort();
        userChild1.setExperience(8);
        userChild1.setSalary(715);
        double wage = userChild1.getSalary();
        double exp = userChild1.getExperience();
        System.out.println("RaisedSalary: "+userChild1.raisedSalary(exp, wage)+ " USD");

    }
}
