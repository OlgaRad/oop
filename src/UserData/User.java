package UserData;

public class User {
   private String firstName;
   private String lastName;
   private int workPhone;
   private long mobilePhone;
   private String email;
   private String password;

    public User(String firstName, String lastName, int workPhone, long mobilePhone, String email, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.workPhone = workPhone;
        this.mobilePhone = mobilePhone;
        this.email = email;
        this.password = password;
    }
    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }
    public String getPassword() {
        return password;
    }

    public void displayInfo() {
       System.out.printf("firstName: %s\nsecondName: %s\nworkPhone: %d\nmobilePhone: %d\nemail: %s\npassword: %s\n\n", firstName,lastName,workPhone,mobilePhone,email,password);
    }

    public void displayInfoShort() {
        System.out.printf("\nemail: %s\npassword: %s\n\n",email,password);
    }

    public boolean isValidPassword() {
        if (password.length()<8 || password.length()>16) {
            return false;
        } else
            return true;
    }

    public boolean isValidEmail(String email) {
      char[] emailArray = email.toCharArray();
        for (int i =0; i<emailArray.length; i++) {
            if (emailArray[i] == '@') {
                return true;
            }
        } return false;

//        if (email.contains("@"))
//            return true;
//            else
//                return false;
    }

    
}

