package UserData;

public class UserChild extends User {
    private double salary;
    private double experience;

    public UserChild(String firstName, String lastName, int workPhone, long mobilePhone, String email, String password, double experience, double salary) {
        super(firstName, lastName, workPhone, mobilePhone, email, password);
        this.salary = salary;
        this.experience = experience;
    }

    public UserChild(String email, String password) {
        super(email,password);
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
    public void setExperience(double experience) {
        this.experience =experience;
    }

    public double getSalary(){
        return salary;
    }
    public double getExperience() {
        return experience;
    }
    public double raisedSalary(double experience, double salary) {
       if (experience<2) {
            this.salary = salary*1.05;
        } else if (experience>=2 || experience<=5) {
            this.salary = salary*1.1;
        } else {
            this.salary = salary*1.15;
        } return this.salary;
    }
}
